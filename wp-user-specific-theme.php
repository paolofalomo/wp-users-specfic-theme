<?php
/**
Plugin Name: WP User Specific Theme
Author: Paolo Falomo
Author Uri: http://www.paolofalomo.com/
Gitlab Uri: https://gitlab.com/paolofalomo/wp-users-specfic-theme
Version: 1.0
*/
class MyThemeSwitcher {
    private $admin_theme = 'admin_theme';
    private $default_theme = 'default_theme';
    private $allowedusers = array(1,905);

    function MyThemeSwitcher() {
        add_filter( 'stylesheet', array( &$this, 'get_stylesheet' ) );
        add_filter( 'template', array( &$this, 'get_template' ) );
    }

    function get_stylesheet($stylesheet = '') {
        if ( is_user_logged_in() && $this->admin_theme && in_array(get_current_user_id( ), $this->allowedusers)) {
            return $this->admin_theme;
        }else{
        	return $this->default_theme;
        }
        return $stylesheet;
    }

    function get_template( $template ) {
        if ( is_user_logged_in() && $this->admin_theme && in_array(get_current_user_id( ), $this->allowedusers)) {
            return $this->admin_theme;
        }else{
        	return $this->default_theme;
        }
        return $template;
    }
}

//$theme_switcher = new MyThemeSwitcher();